module ApplicationHelper
  def voter game
    links = (1..5).map do |n|
      link_to "X", "#", "data-remote-url" => vote_for_game_url(game), "data-rank" => n, :class => "vote #{'current_rank' if @vote_by_user and @vote_by_user.rank >= n}"
    end
    content_tag(:span, links.join.html_safe, :class => "votes #{'voted_by_user' if @vote_by_user}")
  end
end
