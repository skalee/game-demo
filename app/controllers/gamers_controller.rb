class GamersController < ApplicationController
  def create
    @gamer = Gamer.new
    @gamer.password = params[:gamer][:password]
    @gamer.password_confirmation = params[:gamer][:password_confirmation]
    @gamer.email = params[:gamer][:email]
    @gamer.username = params[:gamer][:username]
    @gamer.save!
    reset_session # protect from session fixation attacks;
    auto_login(@gamer)
    redirect_back_or_to root_url, :notice => "Created!"
  end
end
