class SessionController < ApplicationController
  def new
    @gamer = Gamer.new
  end

  def create
    @gamer = login(params[:session][:username], params[:session][:password])
    if @gamer
      redirect_back_or_to root_url, :notice => "Signed in!"
    else
      @gamer = Gamer.new
      flash.now.alert = "Username or password was invalid"
      render :new
    end
  end

  def destroy
    logout
    redirect_to root_url, :notice => "Logged out!"
  end
end
