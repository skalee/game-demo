class GamesController < ApplicationController
  before_filter :find_game, :only => [:show, :vote]

  def index
  end

  def new
    @game = Game.new
  end

  #TODO restrict to logged in
  def create
    @game = Game.new params[:game]
    @game.contributor = current_user
    @game.save!
    redirect_to games_url
  end

  def show
    @vote_by_user = (current_user and @game.votes.where(:gamer_id => current_user.id).first)
  end

  def vote
    vote_ = @game.votes.where(:gamer_id => current_user.id).first_or_initialize
    vote_.rank = params[:rank]
    vote_.save
    render :json => {:status => :ok}
  end

  protected

  def find_game
    @game = Game.find params[:id]
  end
end
