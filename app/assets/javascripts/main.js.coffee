$ ->
  $('a.vote').click ->
    a = $(@)
    $.ajax({ type: 'PUT', url: a.data('remote-url'), data: { rank: a.data('rank') } })
    a.nextAll().removeClass('current_rank')
    a.prevAll().add(a).addClass('current_rank')
    false

