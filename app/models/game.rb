class Game < ActiveRecord::Base
  attr_accessible :description, :title

  belongs_to :contributor, :class_name => Gamer.name
  has_many :votes

  validates_presence_of :title
  validates_uniqueness_of :title
  validates_length_of :title, :maximum => 100

  def avg_rank
    self.votes.average(:rank)
  end
end
