class Vote < ActiveRecord::Base
  belongs_to :gamer
  belongs_to :game

  validates_inclusion_of :rank, :in => 1..5
  #validates_uniqueness_of :gamer_id, :scope => :game_id

  attr_accessible
end
