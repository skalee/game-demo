class Gamer < ActiveRecord::Base
  authenticates_with_sorcery!

  has_many :votes

  validates_presence_of :username
  validates_presence_of :password, :on => :creation
  validates_confirmation_of :password, :on => :creation
  validates_uniqueness_of :username

  attr_accessible

  #TODO db indexes
end
