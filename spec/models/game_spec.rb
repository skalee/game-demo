require 'spec_helper'

describe Game do
  let!(:some_game){ FactoryGirl.create(:game) }

  it { should have_db_column(:title).of_type(:string) }
  it { should have_db_column(:description).of_type(:text) }

  it { should validate_presence_of(:title) }
  it { should validate_uniqueness_of(:title) }

  it { should ensure_length_of(:title).is_at_most(100) }
  it { should_not allow_value("  ").for(:title) }

  it { should belong_to(:contributor).class_name(Gamer) }
  it { should have_many(:votes) }

  it { should_not allow_mass_assignment_of(:contributor) }

  describe "#avg_rank" do
    before do
      FactoryGirl.create(:vote, :game => some_game, :rank => 3)
      FactoryGirl.create(:vote, :game => some_game, :rank => 3)
      FactoryGirl.create(:vote, :game => some_game, :rank => 5)
      FactoryGirl.create(:vote, :game => some_game, :rank => 4)
    end

    it "should return average rank" do
      some_game.avg_rank.should == 3.75
    end
  end
end
