require File.expand_path(File.dirname(__FILE__) + '/../spec_helper')

describe Gamer do
  before { FactoryGirl.create(:gamer) }

  it { should have_db_index(:username).unique(true) }

  it { should validate_presence_of(:username) }
  it { should validate_uniqueness_of(:username) }
  it { should_not allow_mass_assignment_of(:password) }
  it { should_not allow_mass_assignment_of(:password_confirmation) }
  it { should_not allow_mass_assignment_of(:username) }

  it { should have_many(:votes) }
end
