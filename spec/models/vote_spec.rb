require 'spec_helper'

describe Vote do
  before { FactoryGirl.create(:vote) }

  it { should have_db_column(:rank).of_type(:integer) }

  it { should have_db_index([:game_id, :gamer_id]).unique(true) }
  it { should have_db_index(:game_id) }

  it { should belong_to :gamer }
  it { should belong_to :game }
  it { should ensure_inclusion_of(:rank).in_range(1..5) }
end
