require 'spec_helper'

describe GamesController do

  let(:game){ FactoryGirl.create :game }
  let(:gamer){ FactoryGirl.create :gamer }

  describe "GET index" do
    it "should return http success" do
      get 'index'
      response.should be_success
    end

    it "should render index template" do
      get 'index'
      response.should render_template("index")
    end
  end

  describe "GET new" do
    it "should assign @game, render new template" do
      get :new
      assigns(:game).should be_a(Game)
      response.should render_template("new")
    end
  end

  describe "GET show" do
    it "should assign @game, render show template" do
      get :show, :id => game
      assigns(:game).should be_a(Game)
      response.should render_template("show")
    end
    it "should assign nil to @vote_by_user when user has not voted" do
      login_user(gamer)
      get :show, :id => game
      assigns(:vote_by_user).should be_nil
      response.should render_template("show")
    end
    it "should assign @vote_by_user when user has voted" do
      FactoryGirl.create :vote, :game => game, :gamer => gamer
      login_user(gamer)
      get :show, :id => game
      assigns(:vote_by_user).should be_a(Vote)
      response.should render_template("show")
    end
  end

  describe "PUT vote" do
    before{ login_user gamer }
    it "should create new vote if given user never vote for particular game" do
      FactoryGirl.create :vote, :game => game # some other vote for this game
      FactoryGirl.create :vote, :gamer => gamer
      expect{ xhr :put, :vote, :id => game, :rank => 4 }.to change{ Vote.count }.by(1)
      Vote.where(:gamer_id => gamer, :game_id => game).first.rank.should == 4
    end
    it "should update vote if given user already voted for particular game" do
      vote = FactoryGirl.create :vote, :game => game, :gamer => gamer, :rank => 2
      expect{ xhr :put, :vote, :id => game, :rank => 4 }.not_to change{ Vote.count }
      vote.reload.rank.should == 4
    end
  end

end
