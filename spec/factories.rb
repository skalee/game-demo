FactoryGirl.define do
  sequence(:gamer){ |n| "G@m3r #{n}" }
  sequence(:random_string){ |n| "s" * n }

  factory :gamer do
    username{ FactoryGirl.generate(:gamer) }
    password "something"
  end

  factory :game do
    title{ FactoryGirl.generate(:random_string) }
    description{ FactoryGirl.generate(:random_string) }
    association :contributor, :factory => :gamer
  end

  factory :vote do
    association :gamer
    association :game
    rank 3
  end
end

