class CreateVotes < ActiveRecord::Migration
  def change
    create_table :votes do |t|
      t.references :gamer, :null => false
      t.references :game, :null => false
      t.integer :rank, :null => false

      t.timestamps
    end
  end
end
