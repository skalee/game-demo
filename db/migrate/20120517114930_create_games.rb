class CreateGames < ActiveRecord::Migration
  def change
    create_table :games do |t|
      t.string :title, :null => false
      t.text :description
      t.references :gamer, :null => false

      t.timestamps
    end
  end
end
