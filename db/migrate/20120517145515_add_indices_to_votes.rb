class AddIndicesToVotes < ActiveRecord::Migration
  def up
    add_index :votes, [:game_id, :gamer_id], :unique => true
    add_index :votes, [:game_id]
  end

  def down
    remove_index :votes, [:game_id, :gamer_id]
    remove_index :votes, [:game_id]
  end
end
