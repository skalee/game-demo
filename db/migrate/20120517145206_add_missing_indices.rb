class AddMissingIndices < ActiveRecord::Migration
  def up
    add_index :gamers, :username, :unique => true
  end

  def down
    remove_index :gamers, :username
  end
end
