class RenameGamerIdToContributorIdInGames < ActiveRecord::Migration
  def up
    rename_column(:games, :gamer_id, :contributor_id)
  end

  def down
    rename_column(:games, :contributor_id, :gamer_id)
  end
end
